const express =require("express");
const mysql=require("mysql2");
const app=express();
const connection=mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: "node"
})
app.get("/",function(req,res){
    res.send("Hello");
});
app.get("/about",function(req,res){
    res.send("Hi I'm Anurag Vij");
})
const sql=`CREATE TABLE IF NOT EXISTS data(
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    status VARCHAR(255) NOT NULL
)`;
connection.query(sql,function(err,result){
    if(err) throw err;
    console.log("Table created");
});
const select='SELECT * FROM data';
app.get('/todo',(req,res)=>{
    connection.query(select,(err,rows)=>{
        if(err) throw err;
        res.send(rows);
    });
});
app.get('/todo/:id',(req,res)=>{
    const fetch="SELECT * FROM data WHERE id=?";
    const fetchId=req.params.id;
    connection.query(fetch,fetchId,(err,result)=>{
        if(err) throw err;
        res.send(result);
    });
});
app.use(express.json());
app.post('/add', (req, res) => {
    const title=req.body.title;
    const desc=req.body.description;
    const status=req.body.status;
    const insert='INSERT INTO data VALUES(NULL,?,?,?)';
    connection.query(insert, [title,desc,status], (err, result) => {
        if (err) throw err;
        console.log("1 record inserted");
        res.send("Posted");
    });
});
app.delete('/remove/:id',(req,res)=>{
    const delId=req.params.id;
    const del="DELETE FROM data WHERE id=?";
    connection.query(del,delId,(err,result)=>{
        if(err) throw err;
        if(result.affectedRows===0){
            res.send("Id not Present");
        }else{
            res.send("Deleted");
    }
    });
});
app.put('/edit/:id',(req,res)=>{
    const upId=req.params.id;
    const title=req.body.title;
    const desc=req.body.description;
    const status=req.body.status;
    const update="UPDATE data SET title=?,description=?,status=? WHERE id=?";
    connection.query(update,[title,desc,status,upId],(err,result)=>{
        if(err) throw err;
        if(result.affectedRows===0){
            res.send("Id not Present");
        }else{
            res.send("Updated");
        }
    });
});
app.listen(3000,()=>{
    console.log("Server Started");
});
connection.connect((err)=>{
    if(err){
        console.error("Error connecting to database"+err.stack);
        return;
    }
    console.log("Connected to a database as id"+ connection.threadId)
});

